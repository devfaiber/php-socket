# Requisitos
- Composer
- Xampp
- php 7.x

## Ejecucion
para realizar la ejecutacion se hace con el comando
`php serve chat.php`

## Probar funcionalidad
para probar la funcionalidad instalas las dependencias y ya luego al correr el servidor `localhost:3307`.

## Ejecutar el socket puro
para dicha ejecutacion, se encuentra en la carpeta socket_puro_php, y a continuacion `php serve socket.php`

## Pruebas con socket.php
para la prueba se deber realizar en un telnet y con dicho comando `telnet ip:port`

**NOTA:** RECUERDA ESTABLECER LA INFORMACION DE SU IP O UN PUERTO DIFERENTE SI LO DESEEA
contacto: dev.faiber@gmail.com para obtener aclaracion a aprender
