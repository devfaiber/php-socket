<?php

class Connection
{

    private $host;
    private $user;
    private $pwd;
    private $dbname;
    private static $instance;
    private $conexion_db;

    private function __construct(
        $host = "127.0.0.1",
        $port = 3306,
        $dbname = "test",
        $user = "root",
        $pwd = ""
    ){
        $this->conexion_db = new PDO(
            "mysql:host=$host;dbname=$dbname;port=$port;charset=UTF8;unix_socket=/tmp",
            $user,
            $pwd,
            
        );
    }

    public static function getInstance($host = "127.0.0.1", $port = 3306)
    {
        if (self::$instance == null) {
            self::$instance = new Connection($host, $port);
        }
        return self::$instance;
    }

    public function getConexion()
    {
        return $this->conexion_db;
    }
    /**
     * @param sql permite recibir la consulta
     * @param params permite recibir los parametros para la consulta
     */
    public function queryBuilder($sql, $params = [])
    {
        $stmt = $this->conexion_db->prepare($sql);
        
        if (count($params) > 0) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
        
    }

}


?>