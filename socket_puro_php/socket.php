<?php
/**
 * SERVIDOR SOCKET PURO PHP FUNCIONAR VIA USO TELNET, CON IP INDICADA Y PUERTO, RECIBE RESPUESTAS ENTRE USUARIOS POR CONEXION TELNET
 * 
 */

#!/usr/local/bin/php -q
error_reporting(E_ALL);

/* Permitir al script esperar para conexiones. */
set_time_limit(0);

/* Activar el volcado de salida implícito, así veremos lo que estamos obteniendo
 * mientras llega. */
ob_implicit_flush();

class Socket
{
    public $ipv4;
    public $port;
    private $socket_open;
    private const STR_BIENVENIDO = "BIENVENIDO AL SERVIDOR XD";

    public function __construct($ip, $port = 80)
    {
        
        $this->ipv4 = $ip;
        $this->port = $port;
    }
    public function create()
    {
        $this->socket_open = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if ($this->socket_open == false) {
            echo "ERROR: lo sentimos no se ha creado el socket \n";
            echo socket_strerror(socket_last_error($this->socket_open));
        } else {
            echo "SUCCCES: se ha creado exitosamente el socket \n";
        }

        
    }
    public function connect()
    {
        $is_connected = socket_bind($this->socket_open, $this->ipv4, $this->port);

        if ($is_connected == false) {
            echo "ERROR: lo sentimos no se ha podido conectar a la ipv4 ".$this->ipv4.":".$this->port."\n";
            echo socket_strerror(socket_last_error($this->socket_open));
        } else {
            echo "SUCCESS: conexion establecida a la ipv4 ".$this->ipv4.":".$this->port."\n";
        }
    }
    public function listen()
    {
        $is_listen = socket_listen($this->socket_open,10);
        if ($is_listen == false) {
            echo "ERROR: no se puso a la escucha en el puerto ".$this->port."\n";
            echo socket_strerror(socket_last_error($this->socket_open));
        } else {
            echo "SUCCESS: se pone a la escucha en el puerto ".$this->port."\n";
        }
    }
    public function acceptConection()
    {
        $responseClient;
        do {
            $responseClient = socket_accept($this->socket_open);
            
            if ($responseClient == false) {
                echo "ERROR: no se ha aceptado la conexion del cliente";
                break;
            }else{
                echo "Se establecio un nuevo cliente \n";
            }

            socket_write($responseClient,self::STR_BIENVENIDO);

            if ($this->manager($responseClient) == false) {
                break;
            }

        } while (true);
    }

    private function manager(&$responseClient)
    {
        do {
            $buf = socket_read($responseClient,2048,PHP_NORMAL_READ);

            if ($buf == false) {
                return false;
            }elseif (!$buf = trim($buf)) {
                continue;
            }elseif ($buf == 'exit') {
                echo "good";
                break;
            } elseif ($buf == "shutdown") {
                $this->destroy($responseClient);
                return false;
            }

            socket_write($responseClient,"PHP DIGO: ".$buf);
            echo $buf."\n";

        } while (true);
        echo "saliste del chat - - - - - - - ";
        return true;
    }

    public function destroy(&$response = null)
    {
        if ($response !== null) {
            socket_close($response);
        } else {
            socket_close($this->socket_open);
        }
        return true;
    }




}

// instancia
$c = new Socket("127.0.0.1",3306);
$c->create();
$c->connect();
$c->listen();
$c->acceptConection();
$c->destroy();