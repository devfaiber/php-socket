<?php
require __DIR__ . '/vendor/autoload.php';

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;


    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        echo $conn->remoteAddress;
        $this->clients->attach($conn);
        echo "New Connection ID:".$conn->resourceId."\n";
        
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $arr = [
                    "clase"=>"red",
                    "ipv4"=> $from->remoteAddress,
                    "msg"=> "<p class='title'>(".$from->remoteAddress."): </p><p class='descripcion'>".$msg."</p>"
                ];
                // The sender is not the receiver, send to each client connected
                
            } else {
                $arr = [
                    "clase"=>"blue",
                    "ipv4"=> "Yo",
                    "msg"=> "<p class='title'>(Yo): </p><p class='descripcion'>".$msg."</p>"
                ];
            }

            $client->send(json_encode($arr));
        }
        
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}



